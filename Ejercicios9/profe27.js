class Pelicula{
    constructor({id, titulo, director, estreno, pais, generos, calificacion}){
        this.id = id;
        this.titulo = titulo;
        this.director = director;
        this.estreno = estreno;
        this.pais = pais;
        this.generos = generos;
        this.calificacion = calificacion;

        this.validarIMDB(id);
        this.validarTitulo(titulo);
        this.validarDirector(director);
        this.validarEstreno(estreno);
        this.validarGeneros(generos);
        this.validarCalificacion(calificacion);
    }

    static get listaGeneros(){
        return [
            "Action",
            "Adult",
            "Adventure",
            "Animation",
            "Biography",
            "Comedy",
            "Crime",
            "Documentary",
            "Drama",
            "Family",
            "Fantasy",
            "Film Noir",
            "Game-Show",
            "History",
            "Horror",
            "Musical",
            "Music",
            "Mystery",
            "News",
            "Reality-TV",
            "Romance","Sci-Fi",
            "Short",
            "Sport",
            "Talk-Show",
            "Thriller",
            "War",
            "Western"
        ];
    }

    static generosAceptados(){
        return console.info(
            `Los géneros aceptados son: ${Peliculas.listaGeneros.join(", ")}`
        );
    }

    validarCadena(propiedad, valor){
        if(!valor)
            return console.warn(
                `${propiedad} "${valor}" esta vacío`
            );
        if(typeof valor !== "string")
        return console.error(
            `${propiedad}"${valor}" ingresado, NO es una cadena de texto`
        );
        return true;
    }

    validarLongitudCadena(propiedad, valor, longitud){
        if(valor.length > longitud)
            return console.error(
                `${propiedad} "${valor}" excede el número de caracteres permitidos (${longitud})`
            );
        return true;
    }

    validarNumero(propiedad, valor){
        if(!valor)
            return console.warn(
                `${propiedad} "${valor}" ingresado, NO es un número`
            );
        if(typeof valor !== "number")
            return console.error(
                `${propiedad} "${valor}" ingresado, NO es un número`
            );
        return true;
    }

    validarArreglo(propiedad, valor){
        if (!valor) return console.warn(
            `${propiedad} "${valor}" ingresado, NO es un arreglo`
        );

        if(!(valor instanceof Array))
            return console.error(
                `${propiedad} "${valor}" ingresado, NO es un arreglo`
            );
        
        for (let cadena of valor) {
            if(typeof cadena !== "string")
                return console.error(
                    `El valor "${cadena}" ingresado, NO es una cadena de texto`
                );
        }
        return true;
    }

    validarIMDB(id){
        if(this.validarCadena("IMDB id", id)){
            if(!(/^([a-z]){2}([0-9]){7}$/.test(id)))
                return console.error(
                    `IMDB id "${id}" no es valido, debe tener 9 caracteres, los 2 primeros letras minúsculas, los 7 restantes números.`
                );
        }
    }

    validarTitulo(titulo){
        if (this.validarCadena("Titulo", titulo))
            this.validarLongitudCadena("Titulo", titulo, 100);
    }

    validarDirector(director){
        if(this.validarCadena("Director", director))
            this.validarLongitudCadena("Director", director, 50);
    }

    validarEstreno(estreno){
        if(this.validarNumero("Año de Estreno", estreno)){
            if(!(/^([0-9]{4})$/.test(estreno)))
                return console.error(
                    `Año de Estreno "${estreno}" no es válido, de be ser un número de 4 digitos`
                );
        }
    }

    validarPais(pais){
        this.validarArreglo("País", pais);
    }

    validarGeneros(generos){
        if (this.validarArreglo("Generos", generos)){
            for (let genero of generos){
                if(!(Pelicula.listaGeneros.includes(genero))){
                    return console.error(
                        `Genero(s) incorrectos "${generos.join(", ")}"`
                    );
                    Pelicula.generosAceptados();
                }
            }
        }
    }

    validarCalificacion(calificacion){
        if(this.validarNumero("Calificacion", calificacion))
            return (calificacion < 0 || calificacion > 10)
            ? console.error(`La calificacion tiene que estar en un rango entre 0 y 10`)
            : this.calificacion = calificacion.toFixed(1);
    }

    fichaTecnica(){
        console.info(
            `Ficha Tecniva:\n
             Título: "${this.titulo}"\n
             Director: "${this.director}"\n
             Año: "${this.estreno}"\n
             Pais: "${this.pais.join("-")}"\n
             Género(s): "${this.generos.join(", ")}"\n
             Calificación: "${this.calificacion}"\n
             IMDB: "${this.id}"`
        );
    }
}

/* const peli = new Pelicula({
    id: "tt1234567",
    titulo: "Titulo de la peli",
    director: "Director de la peli",
    estreno: 2020,
    pais: ["Mexico"],
    generos: ["Comedy", "Sport"],
    calificacion: 7.789
});

peli.fichaTecnica(); */

const movies = [
    {
      id: "tt0068646",
      titulo: "The Godfather",
      director: "Francis Ford Coppola",
      estreno: 1972,
      pais: ["USA"],
      generos: ["Crime", "Drama"],
      calificacion: 9.2,
    },
    {
      id: "tt0109830",
      titulo: "Forrest Gump",
      director: "Robert Zemeckis",
      estreno: 1994,
      pais: ["USA"],
      generos: ["Drama", "Romance"],
      calificacion: 8.8,
    },
    {
      id: "tt0093773",
      titulo: "Predator",
      director: "John McTiernan",
      estreno: 1987,
      pais: ["USA"],
      generos: ["Action", "Adventure", "Sci-Fi"],
      calificacion: 7.8,
    },
  ];

movies.forEach(el => new Pelicula(el).fichaTecnica());