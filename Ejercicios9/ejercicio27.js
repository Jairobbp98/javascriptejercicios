/* 27) Programa una clase llamada Pelicula.

La clase recibirá un objeto al momento de instanciarse con los siguentes datos: id de la película en IMDB, titulo, director, año de estreno, país o países de origen, géneros y calificación en IMBD.
  - Todos los datos del objeto son obligatorios.
  - Valida que el id IMDB tenga 9 caracteres, los primeros 2 sean letras y los 7 restantes números.
  - Valida que el título no rebase los 100 caracteres.
  - Valida que el director no rebase los 50 caracteres.
  - Valida que el año de estreno sea un número entero de 4 dígitos.
  - Valida que el país o paises sea introducidos en forma de arreglo.
  - Valida que los géneros sean introducidos en forma de arreglo.
  - Valida que los géneros introducidos esten dentro de los géneros aceptados*.
  - Crea un método estático que devuelva los géneros aceptados*.
  - Valida que la calificación sea un número entre 0 y 10 pudiendo ser decimal de una posición.
  - Crea un método que devuelva toda la ficha técnica de la película.
  - Apartir de un arreglo con la información de 3 películas genera 3 
    instancias de la clase de forma automatizada e imprime la ficha técnica 
    de cada película.

* Géneros Aceptados: Action, Adult, Adventure, Animation, Biography, Comedy, Crime, Documentary ,Drama, Family, Fantasy, Film Noir, Game-Show, History, Horror, Musical, Music, Mystery, News, Reality-TV, Romance, Sci-Fi, Short, Sport, Talk-Show, Thriller, War, Western. */

class Peliculas {
  constructor({id, titulo, director, releaseYear, paises, genero, calificacion}){
    this.id = id;
    this.titulo = titulo;
    this.director = director;
    this.releaseYear = releaseYear;
    this.paises = paises;
    this.genero = genero;
    this.calificacion = calificacion;
    this.validarId(id);
    this.validarTitulo(titulo);
    this.validarDirector(director);
    this.validarReleaseYear(releaseYear);
    this.validarPais(paises);
    this.validarGeneros(genero);
    this.validarCalificacion(calificacion);
  }

  static get getListaGeneros(){
    return [
      "Action",
      "Adult",
      "Adventure",
      "Animation",
      "Biography",
      "Comedy",
      "Crime",
      "Documentary",
      "Drama",
      "Family",
      "Fantasy",
      "Film Noir",
      "Game-Show",
      "History",
      "Horror",
      "Musical",
      "Music",
      "Mystery",
      "News",
      "Reality-TV",
      "Romance","Sci-Fi",
      "Short",
      "Sport",
      "Talk-Show",
      "Thriller",
      "War",
      "Western"
    ];
  }

  //validaciones de tipos de datos
  validarNumber(number) {
    if (number === undefined)
      return console.error(
        "No a insertado ningun dato"
        );
    if (typeof number !== "number")
      return console.warn(
        "El dato ingresado no es un numero");
    return true;
  }

  validarStrings(strings) {
    if (strings === undefined)
      return console.error(
        "No a insertado ningun dato"
        );
    if (typeof strings !== "string")
      return console.warn(
        "El dato ingresado no es una cadena de texto"
        );
    return true;
  }

  validarArrays(arrays){
    if (arrays === undefined)
      return console.error(
        "No a insertado ningun dato"
        );
    if (!(arrays instanceof Array))
      return console.warn(
        "El dato ingresado tiene que ser un arreglo"
        );
    return true
  }
  //Metodos
  //Valida que el id IMDB tenga 9 caracteres, los primeros 2 sean letras y los 7 restantes números.
  validarId(id){
    if (id === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if (!(this.validarStrings(id)))
      return console.warn(
        "este valor no es una cadena de texto"
        );
    if(/^([A-Z]|[a-z]){2}([0-9]){7}$/g.test(id) === false)
        return console.warn(
          "Error al ingresar el id. Verifique el id"
        );
    return true;
  }

  validarTitulo(titulo){
    if (titulo === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if(!(this.validarStrings(titulo)))
        return console.warn(
          "este valor no es un string"
        );
    if(titulo.length > 100)
        return console.warn(
          "El titulo no puede tener mas de 100 caracteres"
        );
    return true
  }

  validarDirector(director){
    if (director === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if (!(this.validarStrings(director)))
        return console.warn(
          "este valor no es un string"
        );
    if (director.length > 50)
        return console.warn(
          "El director no puede tener mas de 100 caracteres"
        );
    return true
  }

  validarReleaseYear(releaseYear){
    if (releaseYear === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if (!(this.validarNumber(releaseYear)))
      return console.warn(
        "este valor no es un numero"
      );
    if (releaseYear.toString().length < 4 || releaseYear.toString().length > 4)
      return console.warn(
        "verifique el año ingresado"
      );
    return true;
  }

  validarPais(pais){
    if (pais === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if (!(this.validarArrays(pais)))
      return console.warn(
        "este valor no es un arreglo"
      );
    for (let i of pais) {
      if (!(this.validarStrings(i)))
        return console.warn(
          "verifique el pais o los paises ingresados"
        );
    }
    return true;
  }

  validarGeneros(genero){
    if (genero === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if (!(this.validarArrays(genero)))
      return console.warn(
        "este valor no es un arreglo"
      );
    for (let i of genero) {
      if (!(this.validarStrings(i)))
        return console.warn(
        "este valor no es un arrelgo"
        );
    }
    for (let i of genero) {
      if (!(Peliculas.getListaGeneros.includes(i)))
      return console.warn(
        "Denegado. El genero ingresado no esta en la lista de generos autorizados"
      );
    }
    return true;
  }

  validarCalificacion(calificacion){
    if (calificacion === undefined)
      return console.warn(
        "no se ah ingresado ningun dato"
      );
    if(!(this.validarNumber(calificacion)))
      return console.warn(
        "este valor no es un numero"
      );
    if(calificacion > 10 || calificacion < 0)
      return console.warn(
        "la clasificacion ingresada no es correcta"
      );
    return true;
  }

  informacionPeliculas(){
    return console.info(
      `Tabla de información\n
      IMDB: ${this.id}\n
      Title: ${this.titulo}\n
      Director: ${this.director}\n
      Año: ${this.releaseYear}\n
      Pais: ${this.paises.join("-")}\n
      genero: ${this.genero.join(", ")}\n
      Clasificacion: ${this.calificacion}`
    );
  }
}

const movies = [
  {
    id: "tt0068646",
    titulo: "The Godfather",
    director: "Francis Ford Coppola",
    releaseYear: 1972,
    paises: ["USA"],
    genero: ["Crime", "Drama"],
    calificacion: 9.2,
  },
  {
    id: "tt0109830",
    titulo: "Forrest Gump",
    director: "Robert Zemeckis",
    releaseYear: 1994,
    paises: ["USA"],
    genero: ["Drama", "Romance"],
    calificacion: 8.8,
  },
  {
    id: "tt0093773",
    titulo: "Predator",
    director: "John McTiernan",
    releaseYear: 1987,
    paises: ["USA"],
    genero: ["Action", "Adventure", "Sci-Fi"],
    calificacion: 7.8,
  },
];

movies.forEach((pelis) => new Peliculas(pelis).informacionPeliculas());