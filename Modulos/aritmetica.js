function sumar(a, b) {
    return a + b;
}

function restar(a, b) {
    return a - b;
}

export const aritmetica = {
    sumar,
    restar
}; // si las funciones y las propiedades tienen el mismo nombre se dejan igual.