// Programa una función que dada una cadena de texto cuente el número de vocales y consonantes, pe. miFuncion("Hola Mundo") devuelva Vocales: 4, Consonantes: 5.

const contarVocalesConsonantes = (texto = "") =>{
    if(typeof texto !== "string") return console.log("Ingrese un texto valido");

    let vocales = texto.replace(/ /g, "").toLowerCase().split(""),
        consonantes = texto.replace(/[ aeiou]/g, "").toLowerCase(),
        conteoVocales = 0;

    //Encontrar vocales
    for (let i = 0; i < vocales.length; i++) {
        switch (vocales[i]) {
            case "a":
                conteoVocales++
                break;
            case "e":
                conteoVocales++
                break;
            case "i":
                conteoVocales++
                break;
            case "o":
                conteoVocales++
                break;
            case "u":
                conteoVocales++
                break;
        }
    }
    /* return console.log("vocales: " + conteoVocales); */
    return console.log("El texto es: " + `"${texto}"` + "\n" + `Vocales: ${conteoVocales}` + "\n" + `Consonantes: ${consonantes.length}`);
}

contarVocalesConsonantes("Hola mami como vas");

//solucion del profesor

const contarLetras = (cadena = "") =>{
    let vocales = 0,
        consonantes = 0;

    for (let letra of cadena) {
        if (/[aeiouáéíóú]/.test(letra)) vocales++;
        if (/[bcdfghjklmnñpqrstvwxyz]/.test(letra)) consonantes++;
    }

    return console.info({
        cadena,
        vocales,
        consonantes
    });
}

contarLetras("Hola Mundo");