// Programa una función que valide que un texto sea un email válido, pe. miFuncion("jonmircha@gmail.com") devolverá verdadero.

const validateEmail = (email) =>{
    const resultado = (/\S+@\S+\.\S+/g.test(email.trim()))
    ? "Correo Valido n.n"
    : "Correo no Valido :c";

    return console.log(resultado);
}

validateEmail("jairobenjamin98@gmail.com");

const validarEmail = (email = "") =>{
    if(!email) return console.warn("No ingresaste un email");

    if(typeof email !== "string") return console.error(`El valor "${email}" ingresado, NO es una cadena de texto`);

    let expReg =  /[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/i.test(email);

    return (expReg)
    ? console.info(`"${email}", es un email válido`)
    : console.warn(`"${email}", No es un email válido`);
}

//Fusion 19 y 20

const validarPatron = (cadena = "", patron = undefined) =>{
    if(!cadena) return console.warn("No ingresaste un cadena");

    if(typeof cadena !== "string") return console.error(`El valor "${cadena}" ingresado, NO es una cadena de texto`);

    if (patron === undefined) return console.warn("No ingresastes un patrón a evaluar");

    if(!(patron instanceof RegExp)) return console.error(`El valor "${patron}" ingresado, NO es una expresion regular`);

    let expReg = patron.test(cadena);

    return (expReg)
    ? console.info(`"${cadena}", cumple con el patrón ingresado`)
    : console.warn(`"${cadena}", NO cumple con el patron ingresado`);
}