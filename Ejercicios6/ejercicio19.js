// Programa una función que valide que un texto sea un nombre válido, pe. miFuncion("Jonathan MirCha") devolverá verdadero.

const nombreValido = (textoNombre = "") =>{

    if(typeof textoNombre !== "string") return console.log("este no es un texto valido")

    if(/[0-9]/g.test(textoNombre)) return console.log("un nombre no puede llevar numeros");

    if(textoNombre === "") return console.log("Porfavor ingrese un texto valido");

    arregloNombre = textoNombre.split(" ");
    conteoNombre = 0;

    for(let i = 0; i < arregloNombre.length; i++){
        if (/[A-Z]/g.test(arregloNombre[i])) {
            conteoNombre++;
        }
     }

    const resultado = (conteoNombre === arregloNombre.length)
    ? "Su nombre es correcto"
    : "Este no es un nombre porfavor verifique"

    return console.log(resultado + ": " + textoNombre);
}

nombreValido("Jairo Benjamin");

//solucion del profesor

const validarNombre = (nombre = "") =>{
    if(!nombre) return console.warn("No ingresaste un nombre");

    if(typeof nombre !== "string") return console.error(`El valor "${nombre}" ingresado, NO es una cadena de texto`);

    let expReg = /^[A-Za-zÑñÁáÉéÍíÓóÚú\s]+$/g.test(nombre);

    return (expReg)
    ? console.info(`"${nombre}", es un nombre válido`)
    : console.warn(`"${nombre}", No es un nombre válido`);
}

validarNombre("Jairo benjamin");