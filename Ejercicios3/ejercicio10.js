// 10) Programa una función que reciba un número y evalúe si es capicúa o no (que se lee igual en un sentido que en otro), pe. miFuncion(2002) devolverá true.
const prueba = (numero = 0) => {
  if (!numero) return console.info("no ah ingresado ningun numero");

  let cadena = parseInt(numero.toString().split("").reverse().join(""));

  return (cadena === numero)
      ? console.info(`El numero ${numero} es capicúa`)
      : console.info("No es capicúa");
}

prueba(12233221);

//Solucion del profesor

const capicua = (numero = 0) =>{
  if(!numero) return console.warn("No infresaste un numero");

  if(typeof numero !== "number") return console.error(`El valor "${numero}" no es un numero`);

  numero = numero.toString();
  let alReves = numero.split("").reverse().join("");

  return (numero === alReves)
    ? console.log("Si, si es capicua")
    : console.info("No, no es capicua");
}

capicua(212.212)