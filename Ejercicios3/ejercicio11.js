// 11) Programa una función que calcule el factorial de un número (El factorial de un entero positivo n, se define como el producto de todos los números enteros positivos desde 1 hasta n), pe. miFuncion(5) devolverá 120. */

const factor = (numero) =>{
    if (Math.sign(numero) === -1 || Math.sign(numero) === 0) return console.warn("el factorial no puedo 0 ni menor a 0");
    let resultado = 1;
    for(let i = 1; i <= numero; i++){
        resultado *= i;
    }
    return console.log(resultado);
}

factor(6);

//Solucion del profesor

const factorial = (numero = undefined) =>{
    if (numero === undefined) return console.warn("No ingresastes un numero");
    if(typeof numero !== "number") return console.error(`El valor "${numero}" ingresado, NO es un número`);
    if(numero === 0) return console.error("El numero no puede ser negativo");

    let factorial = 1;

    for (let i = numero; i > 1; i--) {
        factorial *= i;
    }

    return console.info(`El factorial de ${numero} es ${factorial}`);
}

factorial(5);