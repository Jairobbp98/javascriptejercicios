//22) Programa una función que dado un array devuelva el número mas alto y el más bajo de dicho array, pe. miFuncion([1, 4, 5, 99, -60]) devolverá [99, -60].

const numeroAltoBajo = (arreglo) =>{
    if(!(arreglo instanceof Array)) return console.warn("Solo se aceptan arreglos de numero");
    
    let nuevoArreglo = [Math.min(...arreglo), Math.max(...arreglo)];

    return console.log(nuevoArreglo);
}

numeroAltoBajo([0,1,2,3,4]);

//solucion del profesor

const arrayMinMax = (arr = undefined) =>{
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    for(let num of arr){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }

    return console.info(`Arreglo original: ${arr}\nValor mayor: ${Math.max(...arr)}.\nValor menor: ${Math.min(...arr)}.`);
}

arrayMinMax([1,4,5,99,-60]);