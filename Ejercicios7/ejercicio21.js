//21) Programa una función que dado un array numérico devuelve otro array con los números elevados al cuadrado, pe. mi_funcion([1, 4, 5]) devolverá [1, 16, 25].

const numerosCuadrados = (arreglo = undefined) =>{
    if(!(arreglo instanceof Array)) return console.warn("Solo se aceptan arreglos de numero");

    for (const i of arreglo) {
        if(typeof i !== "number") return console.error("el valor ingresado no es un arreglo");
    }

    let arregloCuadrado = [];

    for(let i = 0; i < arreglo.length; i++){
        arregloCuadrado.push(Math.pow(arreglo[i], 2));
    }

    return console.log(arregloCuadrado);
}

numerosCuadrados([2,2,2]);

//solucion del profesor

const devolverCuadrados = (arr = undefined) =>{
    if(arr === undefined) return console.warn("No ingresaste un arreglo de números");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    for (let num of arr){
        if(typeof num !== "number") return console.error(`El valor "${num}" ingresado, NO es un número`);
    }

    const newArr = arr.map(el => el * el);

    return console.info(`Arreglo original: ${arr},\nArreglo elevado al cuadrado: ${newArr}`);
}

devolverCuadrados([2,2,2]);