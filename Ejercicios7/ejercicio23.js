//23) Programa una función que dado un array de números devuelva un objeto con 2 arreglos en el primero almacena los números pares y en el segundo los impares, pe. miFuncion([1,2,3,4,5,6,7,8,9,0]) devolverá {pares: [2,4,6,8,0], impares: [1,3,5,7,9]}.

const numeroParesImpares = (arreglo) =>{
    if(!(arreglo instanceof Array)) return console.warn("Solo se aceptan arreglos de numero");

    let pares = [],
        impares = [];

    for (let i = 0; i < arreglo.length; i++) {
        if ((arreglo[i] % 2) === 0){
            pares.push(arreglo[i]);
        }else{
            impares.push(arreglo[i]);  
        }
    }

    return console.log("Arreglo: " + arreglo + "\n" + "Pares: " + pares + "\n" + "Impares: " + impares);
}

numeroParesImpares([1,2,3,4,5]);

const separarParesImpares = (arr = undefined) =>{
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    for(let num of arr){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }

    return console.info({
        pares: arr.filter(num => num % 2 === 0),
        impares: arr.filter(num => num % 2 === 1)
    });
}

separarParesImpares([1,2,3,4,5]);