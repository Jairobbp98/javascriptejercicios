//Operador Ternario
/* let edad = 17;
let eresMayor = (edad >= 18) ? "Eres Mayor de Edad" : "Eres Menor de Edad"; */

//switch
/* let dia = 2;

switch (dia) {
    case 0:
        console.log("Domingo");
        break;
    case 1:
        console.log("Lunes");
        break;
    case 2:
        console.log("Martes");
        break;
    case 3:
        console.log("Miercoles");
        break;
    case 4:
        console.log("Jueves");
        break;
    case 5:
        console.log("Viernes");
        break;
    case 5:
        console.log("Sabado");
        break;
    default:
        console.log("El dia no eiste");
        break;
} */

//ciclo forin: recorre los atributos y propiedades de un objeto
/* const Matriz = {
    nombre : "Jairo",
    apellido : "Barberena",
    edad : 22
}
for (const i in Matriz) {
    console.log(`Key: ${i}, Values: ${Matriz[i]}`);
} */

//ciclo forof: Recorrer arreglos que sean iterables
/* const Matriz = [1, 2, 3, 4, 5, 6, 7, 8, 9];
for (const i of Matriz) {
    console.log(i);
} */

//Destructuración
/* const numeros = [1, 2, 3]; */
//sin destructurar
/* let uno = numeros[0], dos = numeros[1], tres = numeros[2];
console.log(uno, dos, tres); */
//con Destructuracion
/* const [one, two, three] = numeros;
console.log(one, two, three); */
//Destructuracion con objetos "LAS VARIABLS TIENEN QUE TENER EL MISMO NOMBRE QUE LOS ATRIBUTOS DEL OBJETO"
/* let persona = {
    nombre: "jairo",
    apellido: "barberena",
    edad: 35
}

let {nombre, apellido, edad} = persona;
console.log(nombre, apellido, edad); */

//Parametos REST (...x) & Operador Spread

/* function sumar(a, b, ...c) {
    let resultado = a + b;

    c.forEach(function (n) {
        resultado += n
    });
    
    return resultado;
}

console.log(sumar(1, 2));
console.log(sumar(1, 2, 3));
console.log(sumar(1, 2, 3, 4));
console.log(sumar(1, 2, 3, 4, 5)); */

/* const arr1 = [1,2,3,4,5]
const arr2 = [6,7,8,9,10]
const arr3 = [...arr1, ...arr2]
console.log(arr3); */

//Arrow Function

/* const saludar = nombre => console.log("Hola " + nombre);
const sumar = (a, b) => a+b;

console.log(sumar(1, 2)); */

//Prototipos

/* function Animal(nombre, genero) {
    this.nombre = nombre;
    this.genero = genero;
}

Animal.prototype.sonar = function () {
    console.log("Hago sonidos porque estoy vivo");
}

Animal.prototype.saludar = function () {
    console.log(`Hola me llamo ${this.nombre}`)
}

const snoopy = new Animal("Snoopy", "Macho");
console.log(snoopy); */

//clases

/* class Animal{
    constructor(Nombre, Genero){
        this.Nombre = Nombre;
        this.Genero = Genero;
    }

    saludar(){
        console.log(`Hola Me Llamo ${this.Nombre}`);
    }

    sonar(x){
        console.log(`${x}`);
    }
}

class Perro extends Animal{
    constructor(Nombre, Genero, Tamanio){
        super(Nombre, Genero);
        this.Tamanio = Tamanio;
    }

    get getNombre(){
        return this.Nombre;
    }

    set setNombre(Nombre){
        this.Nombre = Nombre;
    }

    saludar(){
        console.log(`Hola Me Llamo ${this.Nombre}`);
    }

    sonar(x){
        console.log(`${x}`);
    }

    static queEres(){
        console.log("Los perros somos animales mamiferos que pertenecemos a la familia de los caninos. Somos considerados el mejor amigo del hombre.");
    }
}

let Per = new Perro();
Per.setNombre = "Scooby-Doo";
console.log(Per.getNombre); */

//objeto consola

/* console.error("Esto es un error");

let Jairo = {
    Nombre : "Jairo",
    Apellido : "Barberena",
    Numero : 76789435
  }
  
console.table(Object.entries(Jairo));

console.time("Cuanto tiempo tarda mi codigo");
const arreglo = Array(1000000);

for (let i = 0; i < arreglo.length; i++) {
    arreglo[i] = i;
}

console.timeEnd("Cuanto tiempo tarda mi codigo");


let x = 1, y = 2, PruebaXY = "Se espera que X siempre sea menor que Y";

console.assert(x < y, {x, y, PruebaXY}); */

//Objeto Date
/* let fecha = new Date();

console.log(fecha) //Sun Aug 16 2020 03:01:43 GMT-0600 (GMT-06:00)
console.log(fecha.getDate()) //dia del mes
console.log(fecha.getDay()); // dia de la semana D L M Mi J V S -> 0 1 2 3 4 5 6
console.log(fecha.getMonth()); //numero del mes ENERO = 0, DICIEMBRE = 11
console.log(fecha.getFullYear()); //Año actual
console.log(fecha.getHours()); // Hora actual
console.log(fecha.getMinutes());// minutos actuales
console.log(fecha.getSeconds()); //segundos actuales
console.log(fecha.toString()); // Sun Aug 16 2020 03:18:29 GMT-0600 (GMT-06:00)
console.log(fecha.toDateString()); //Sun Aug 16 2020
console.log(fecha.toLocaleString()); // 2020-8-16 3:20:04
console.log(fecha.toLocaleDateString()); //2020-8-16
console.log(fecha.toLocaleTimeString()); // 3:21:44
console.log(fecha.getTimezoneOffset()); */

//Objeto Math

/* console.log(Math);
console.log(Math.PI);
console.log(Math.abs(-5)); //Devuelve el valor absoluto de un numero
console.log(Math.ceil(5.6)); // redondea siempre al numero entero mayor
console.log(Math.floor(5.6)); //lo contrario a ceil
console.log(Math.round(5.6)); //redonde al mas cercano
console.log(Math.sqrt(81)); //Raiz cuadrada
console.log(Math.pow(2,5)); //exponencial 2^5
console.log(Math.sign(-7.8)); //verifica si el numero es positivo(1), negativo(-1) o 0(0)
console.log(Math.random()); //numero aleatorio entre 0 y 1
console.log(Math.round(Math.random() * 1000)); */

//Expresiones Regulares

/* let cadena = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id finibus nisl, sit amet efficitur eros. Aenean justo 98 ex, vehicula eu rutrum ac, posuere nec sem. Donec eu accumsan nibh, ac imperdiet sem. Ut sit lorem porttitor ipsum. Vestibulum aliquam nibh et tellus rhoncus mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc finibus condimentum ex non congue. "; */

/* let expReg = new RegExp("lorem", "i");
console.log(expReg.test(cadena));
console.log(expReg.exec(cadena)); */

/* let expReg2 = /[0-9]/ig;
console.log(expReg2.test(cadena));
console.log(expReg2.exec(cadena)); */

