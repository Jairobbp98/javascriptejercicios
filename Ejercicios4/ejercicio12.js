//Programa una función que determine si un número es primo (aquel que solo es divisible por sí mismo y 1) o no, pe. miFuncion(7) devolverá true.

const numeroPrimo = (numero) =>{
    if(typeof numero !== "number") return console.error(`El valor "${numero}" no es un numero`);
    if (!numero) return console.info("No ah ingresado ningun numero");
    if (numero <= 0) return console.info("0 no es un numero primo");

    numero = numero % 2;

    return (numero === 1) 
        ? console.log("El numero es primo")
        : console.log("El numero no es primo");
}

numeroPrimo(19);

//Solucion del profesor

const numeroPrimo = (numero = undefined) => {
    if (typeof numero !== "number") return console.error(`El valor "${numero}" no puede ser ingresado, No es un numero`);

    if(numero === 0) return console.error("El numero no puede ser 0");

    if(numero === 1) return console.error("El numero no puede ser negativo");

    let divisible = false;

    for(let i = 2; i < numero; i++){
        if((numero % i) === 0){
            divisible = true;
            break;
        }
    }
    return
}