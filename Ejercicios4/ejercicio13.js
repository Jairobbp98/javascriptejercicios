//13) Programa una función que determine si un número es par o impar, pe. miFuncion(29) devolverá Impar.

const numerosPares = (numero) =>{
    if(typeof numero !== "number") return console.error(`El valor "${numero}" no es un numero`);
    if (!numero) return console.info("No ah ingresado ningun numero");
    if (numero <= 0) return console.info("0 no es un numero primo");

    numero = numero % 2;

    return (numero === 0) 
        ? console.log("El numero es par")
        : console.log("El numero es impar");
}

numerosPares(11999);

//Solucion del profesor

const numeroParImpar = (numero = undefined) =>{
    if (numero === undefined) return console.warn("No ingresastes un número");

    if (typeof numero !== "number") return console.error(`El valor "${numero}" ingresado, No es un número`);

    return ((numero % 2) === 0)
        ? console.info(`El número ${numero} es Par`)
        : console.info(`El número ${numero} es Par`);
}