//14) Programa una función para convertir grados Celsius a Fahrenheit y viceversa, pe. miFuncion(0,"C") devolverá 32°F.

const convertidorCF = (numero = undefined, unidad = undefined) => {
    if (numero === undefined) return console.info("No ah ingresado ningun numero");
    
    if(typeof numero !== "number") return console.error(`El valor "${numero}" no es un numero`);

    if (unidad === undefined) return console.warn("No ingresastes el tipo de grado a convertir");

    if(typeof unidad !== "string") return console.error(`El valor "${unidad}" ingresado, NO es una cadena de texto`);

    if (unidad === "F") {
        return console.log(`${numero}°F = ${Math.round(((numero-32)*5)/9)}°C`);
    } else if (unidad === "C"){
        return console.log(`${numero}°C = ${Math.round(((numero * 9) / 5) + 32)}°F`);
    }
}

convertidorCF(100, 'F');

//Solucion del profesor
const convertirGrados = (grados = undefined, unidad = undefined) =>{
    if (grados === undefined) return console.warn("No ingresastes grados a convertir");

    if (typeof grados !== "number") return console.error(`El valor ${grados} ingresado, No es un número`);

    if (unidad === undefined) return console.warn("No ingresastes el tipo de grado a convertir");

    if(typeof unidad !== "string") return console.error(`El valor "${unidad}" ingresado, NO es una cadena de texto`);

    if(unidad.length !== 1 || !/(C|F)/.test(unidad)) return console.warn("Valor de unidad no reconocido");

    if (unidad === "C"){
        return console.info(`${grados}°C = ${Math.round((grados * (9/5))) + 32}°F`);
    }else if (unidad === "F"){
        return console.info(`${grados}°F = ${Math.round(((grados - 32) * (5/9)))}°C`);
    }else{
        return console.error("El tipo de grados a convertir NO es válido");
    }
}

convertirGrados(100, "F");