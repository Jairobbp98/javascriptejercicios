/* 16) Programa una función que devuelva el monto final después de aplicar un descuento a una cantidad dada, pe. miFuncion(1000, 20) devolverá 800. */

const montoFinal = (monto = undefined, descuento = undefined) => {
  if (typeof monto !== "number")
    return console.log("Porfavor ingrese el monto en numeros");
  if (typeof descuento !== "number")
    return console.log("Porfavor ingrese el descuento en numeros");
  if (descuento > 100 || descuento < 0)
    return console.log("El descuento no puede ser menor a 0 ni mayor a 100%");

  return console.log(monto - (monto * (descuento / 100)));
};

montoFinal(1000, 20);

//solucion del profesor

const aplicarDescuento = (monto = undefined, descuento = 0) => {
    return console.info(`${monto} - ${descuento}% = ${monto - ((monto * descuento)/100)}`);
}