/* 17) Programa una función que dada una fecha válida determine cuantos años han pasado hasta el día de hoy, pe. miFuncion(new Date(1984,4,23)) devolverá 35 años (en 2020). */

const calcularEdad = (years = 0, month = 0, day = 0) =>{
    if (typeof years !== "number") return console.log("Ingrese el año correctamente");
    if (typeof month !== "number") return console.log("Ingrese el mes correctamente");
    if (typeof day !== "number") return console.log("Ingrese el dia correctamente");
    if (years < 0) return console.log("No se aceptan numeros negativos");
    if (month < 0) return console.log("No se aceptan numeros negativos");
    if (day < 0) return console.log("No se aceptan numeros negativos");

    let fechaActual = new Date(),
        fechaNac = new Date(years, month, day),
        edad;

    edad = fechaActual.getFullYear() - fechaNac.getFullYear();

    if(fechaNac.getMonth() > fechaActual.getMonth()) edad--;

    if (fechaNac.getMonth() === fechaActual.getMonth()){
        if (fechaNac.getDate() > fechaActual.getDate()){
            edad--;
        }    
    }

    return console.log("Su edad actual es: " + edad + " en " + fechaActual.getFullYear());
}

calcularEdad(1998, 5, 2);

//Solucion del profesor

const calcularAnios = (fecha = undefined) =>{
    if (fecha === undefined) return console.warn("No ingresaste la fecha");

    if(!fecha instanceof Date) return console.log("El valor que ingresastes no es una fecha valida");

    let hoyMenosFecha = new Date().getTime() - fecha.getTime(),
        aniosEnMS = 1000 * 60 * 60 * 24 * 365, //milisegundos, segundos, minutos, horas, año
        aniosHumanos = Math.floor(hoyMenosFecha / aniosEnMS);

    return (Math.sign(aniosHumanos) === -1)
            ? console.info(`Faltan ${Math.abs(aniosHumanos)} años para el ${fecha.getFullYear()}.`)
            : (Math.sign(aniosHumanos) === 1)
                ? console.info(`Han pasado ${aniosHumanos} años, desde ${fecha.getFullYear()}.`)
                : console.info(`Estamos en el año actual ${fecha.getFullYear()}.`);
}