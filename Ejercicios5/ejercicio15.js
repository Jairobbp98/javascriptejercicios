/* 15) Programa una función para convertir números de base binaria a decimal y viceversa, pe. miFuncion(100,2) devolverá 4 base 10. */

const conversionNumeros = (numero = "", base = undefined) =>{
    if (numero === "" || base === undefined) return console.info("El numero o base no han sido ingresados correctamente");

    if (typeof numero !== "string") return console.info("El numero ingresado debe ser una cadena");

    if (/[0-9]/g.test(numero) === false) return console.info("El numero que ah intentado ingresar no es correcto");

    let resultado;

    switch (base) {
        //binario a decimal
        case 2:
            if (/0|1/g.test(numero) === true){
               resultado = "El numero binario en decimal es: " + parseInt(numero, base) + " y de decimal a binario es: " + `${parseInt(numero, base).toString(2)}`;
            }
            break;
        case 8:
            if (/0|1/g.test(numero) === true){
               resultado = "El numero octal en decimal es: " + parseInt(numero, base) + " y de decimal a octal es: " + `${parseInt(numero, base).toString(8)}`;
            }
            break;
        case 16:
            if (/0|1/g.test(numero) === true){
                resultado = "El numero hexadecimal en decimal es: " + parseInt(numero, base) + " y de decimal a hexadecimal es: " + `${parseInt(numero, base).toString(16)}`;
            }
            break;
        default:
            console.log("Error, El numero binario ingresado no es valido")
            break;
    }

    return console.log(resultado);
}

conversionNumeros("1100", 2);

//solucion del profesor

const convertirBinarioDecimal = (numero = undefined, base = undefined) =>{
    if (base === 2) {
        return console.info(`${numero} base ${base} = ${parseInt(numero, base)} base 10`);
    }else if (base === 10) {
        return console.info(`${numero} base ${base} = ${numero.toString(2)} base 2`);
    }
}