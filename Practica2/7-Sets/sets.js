//tipo de dato parecido al arreglo pero no permite datos repetidos
const SET = new Set([1,2,3,3,4,5,true,true,false,false,{}, {}]);

console.log(SET);
console.log(SET.size); // funciona com el length

for (const item of SET) {
    console.log(item);
}

SET.forEach(item => console.log(item)); //ser puede usar foreach

let arr = Array.from(SET); //convierte de tipo Set a Array
console.log(arr[0]);

SET.delete(1); //Borrar elemento del Set
console.log(SET);

SET.clear(); //borrar todos los datos que hay en Set