const $whatisDOM = document.getElementById("que-es");

let text = `
    <p>Lorem ipsum dolor sit amet <b><i>consectetur adipisicing elit</b></i>. Molestias reiciendis at possimus eius necessitatibus provident cupiditate</p>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias reiciendis at possimus eius necessitatibus provident cupiditate</p>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias reiciendis at possimus eius necessitatibus provident cupiditate</p>
`;

/* $whatisDOM.innerText = text; */ //ya no lo usa nadie solo se usaba en internet explorer

/* $whatisDOM.textContent = text; */ //no interpreta las etiquetas html del contenido

$whatisDOM.innerHTML = text;
$whatisDOM.outerHTML = text; //remplasa la etiqueta HTML