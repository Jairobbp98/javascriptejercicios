const d = document;
var x = 0,
    y = 0;

const moveBall = (evento, stage, ball) => {
  const $ball = d.querySelector(ball),
        $stage = d.querySelector(stage),
        limitsBall = $ball.getBoundingClientRect(),
        limitsStage = $stage.getBoundingClientRect();

  /* const move = (direction) =>{
    
  } */

  switch (evento.keyCode) {
    //izquierda
    case 37:
      /* move("left"); */

      if (limitsBall.left > limitsStage.left)
      {
        evento.preventDefault();
        x--;
      }
      break;

    //arriba
    case 38:
      /* move("up"); */

      if (limitsBall.top > limitsStage.top)
      {
        evento.preventDefault();
        y--;
      }
      break;

    //derecha
    case 39:
      /* move("right"); */

      if (limitsBall.right < limitsStage.right)
      {
        evento.preventDefault();
        x++;
      }
      break;

    //abajo
    case 40:
      /* move("down"); */

      if (limitsBall.bottom < limitsStage.bottom)
      {
        evento.preventDefault();
        y++;
      }
      break;

    default:
      break;
  }

  $ball.style.transform = `translate(${x * 10}px, ${y * 10}px)`
};

const shortcuts = (e) => {
  /*   console.log(e);
  console.log(e.type);
  console.log(e.key); */
  console.log(e.keyCode);

  if (e.key === "a" && e.altKey) alert("Haz lanzado una alerta con el teclado");
  if (e.key === "q" && e.ctrlKey)
    confirm("Haz lanzado una confirmacion con el teclado");
  if (e.key === "e" && e.shiftKey)
    prompt("Haz lanzado una aviso con el teclado");
};

export { shortcuts, moveBall };
