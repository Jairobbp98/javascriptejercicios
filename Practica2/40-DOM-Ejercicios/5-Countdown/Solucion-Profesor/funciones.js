import { countdown } from "./modules/cuenta_regresiva.js";
import hamburguerMenu from "./modules/menuHamburguesa.js";
import { alarm, digitalClock } from "./modules/reloj.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) =>{
    hamburguerMenu(".panel-btn",".panel", ".menu a");
    digitalClock("#reloj", "#activar-reloj", "#desactivar-reloj");
    alarm("music/alarma-good-4-morning.mp3", "#activar-alarma", "#desactivar-alarma");
    countdown("countdown", "May 23, 2021 03:23:19", "Feliz Cumpleaños Amigo 🎁");
});