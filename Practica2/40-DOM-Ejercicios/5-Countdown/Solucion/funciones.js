import { countdown } from "./countdown.js";
const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  countdown(".Dias", ".Horas", ".Minutos", ".Segundos");
});
