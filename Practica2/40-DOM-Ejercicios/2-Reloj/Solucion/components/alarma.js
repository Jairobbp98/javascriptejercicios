const d = document;

const iniciarAlarma = (iniciarBtn, audio) =>{
    d.addEventListener("click", (e) =>{
        if(e.target.matches(iniciarBtn)){
            d.querySelector(audio).play();
        }
    });
};

const detenerAlarma = (detenerBtn, audio) =>{
    d.addEventListener("click", (e) =>{
        if(e.target.matches(detenerBtn)){
            d.querySelector(audio).pause();
        }
    });
};

export { iniciarAlarma, detenerAlarma };