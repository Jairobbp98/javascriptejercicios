const d = document;
var timer;

const iniciarReloj = (iniciarTiempo, tHora, tMinuto, tSegundo) => {
  d.addEventListener("click", (e) => {
    if (e.target.matches(iniciarTiempo)) {
      timer = setInterval(() => {
        var tiempo = new Date();
        d.querySelector(tHora).textContent = tiempo.getHours();
        d.querySelector(tMinuto).textContent = tiempo.getMinutes();
        d.querySelector(tSegundo).textContent = tiempo.getSeconds();
      }, 1000);
    }
  });
};

const detenerReloj = (detenerTiempo) =>{
  d.addEventListener("click", (e) =>{
    if(e.target.matches(detenerTiempo)){
      clearInterval(timer);
    }
  });
}

export { iniciarReloj, detenerReloj };
