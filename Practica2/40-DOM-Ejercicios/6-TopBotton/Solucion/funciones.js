const d = document;
const buttonHamburguer = document.getElementById("botonHamburguer");
const buttonTop = document.querySelector(".boton-up");
const menu = document.querySelector(".menuLista");

function scroll(){
    if (d.body.scrollTop > 200 || d.documentElement.scrollTop > 200) {
        buttonTop.classList.remove("desaparecer");
    } else {
        buttonTop.classList.add("desaparecer");
    }
}

d.addEventListener("DOMContentLoaded", (e) =>{
    buttonTop.classList.add("desaparecer");
    window.onscroll = function (){scroll()};
});

buttonHamburguer.addEventListener("click", () =>{
    buttonHamburguer.classList.toggle("is-active");
    menu.classList.toggle("mostrarMenuLista");
});

buttonTop.addEventListener("click", () =>{
    d.body.scrollTop = 0;
    d.documentElement.scrollTop = 0;
});