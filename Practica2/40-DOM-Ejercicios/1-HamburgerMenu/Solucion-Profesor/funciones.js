import hamburguerMenu from "./modules/menuHamburguesa.js";

const d = document;

d.addEventListener("DOMContentLoaded", () =>{
    hamburguerMenu(".panel-btn",".panel", ".menu a");
});