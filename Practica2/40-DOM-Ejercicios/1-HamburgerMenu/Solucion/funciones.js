const button = document.getElementById("botonHamburguer");
const menu = document.querySelector(".menuLista");

button.addEventListener("click", () =>{
    button.classList.toggle("is-active");
    menu.classList.toggle("mostrarMenuLista");
});