const d = document;

const countdown = (id, limitDate, finalMessage) => {
  const $countdown = d.getElementById(id),
    $countdownDate = new Date(limitDate).getTime();

  let countdownTempo = setInterval(() => {
    let now = new Date().getTime(),
      limitTime = $countdownDate - now,
      Days = Math.floor(limitTime / (1000 * 60 * 60 * 24)),
      Hours = (
        "0" +
        Math.floor((limitTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60 * 24))
      ).slice(-2),
      Minutes = (
        "0" + Math.floor((limitTime % (1000 * 60 * 60)) / (1000 * 60))
      ).slice(-2),
      Seconds = ("0" + Math.floor((limitTime % (1000 * 60)) / 1000)).slice(-2);

    $countdown.innerHTML = `<h3>Faltan: ${Days} dias ${Hours} horas ${Minutes} minutos ${Seconds} segundos</h3>`;

    if (limitTime < 0) {
      clearInterval(countdownTempo);
      $countdown.innerHTML(`<h3>${finalMessage}</h3>`);
    }
  }, 1000);
};

export { countdown };
