import hamburguerMenu from "./modules/menuHamburguesa.js";
import { alarm, digitalClock } from "./modules/reloj.js";
import { shortcuts } from "./modules/teclado.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) =>{
    hamburguerMenu(".panel-btn",".panel", ".menu a");
    digitalClock("#reloj", "#activar-reloj", "#desactivar-reloj");
    alarm("music/alarma-good-4-morning.mp3", "#activar-alarma", "#desactivar-alarma")
});

d.addEventListener("keydown", (e) => {
    shortcuts(e);
});