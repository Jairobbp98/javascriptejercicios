import { darkModeBtn, Storage } from "./Modules/darkMode.js";
import { links } from "./Modules/links.js";
import { ScrollBotton } from "./Modules/topBotton.js";
const buttonHamburguer = document.getElementById("botonHamburguer");
const menu = document.querySelector(".menuLista");
const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  Storage();
  ScrollBotton(".boton-up");
  darkModeBtn(".dark-mode", "dark");
  links("formulario");
});

buttonHamburguer.addEventListener("click", () => {
  buttonHamburguer.classList.toggle("is-active");
  menu.classList.toggle("mostrarMenuLista");
});
