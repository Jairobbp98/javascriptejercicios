const d = document, n = navigator;

const getGeolocation = (id) => {
    const $id = d.getElementById(id),
          options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
          };

    const success = position => {
        let coords = position.coords;
        console.log(position);

        $id.innerHTML = `
            <p>Tu Posicion actual es:</p>
            <ul>
                <li><b>${coords.latitude}</b></li>
                <li><b>${coords.longitude}</b></li>
                <li><b>${coords.accuracy}</b> Metros</li>
            </ul>
            <a href="https://www.google.com/maps/@${coords.latitude},${coords.longitude},20z" target="_blank" rel="noopener">Ver en google maps</a>
        `;
    };

    const error = err => {
        $id.innerHTML = `<p><mark>Error: ${err.code}, ${err.message}</mark></p>`
        console.log(err.message);
    };

    n.geolocation.getCurrentPosition(success, error, options);
}

export { getGeolocation }