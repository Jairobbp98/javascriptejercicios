const d = document;
const local = localStorage;

const Storage = () => {
  let item = local.getItem("theme");
  const darkItem = d.querySelector(".dark-mode");

  if (item !== null) {
    d.body.classList.add(item);
    darkItem.textContent = "🌞";
  } else {
    darkItem.textContent = "🌚";
  }
};

const darkModeBtn = (btn, darkClase) => {
  const darkBoton = d.querySelector(btn);

  darkBoton.addEventListener("click", (e) => {
    if (e.target.matches(btn)) {
        d.body.classList.toggle('dark');
        local.setItem("theme", (local.getItem("theme") == null ? 'dark' : null));
        darkBoton.textContent = (d.body.classList.contains("dark") ? "🌞" : "🌚")
    }
  });
};

export { darkModeBtn, Storage };