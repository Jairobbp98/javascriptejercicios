const d = document;
const local = localStorage;

const Storage = () => {
  let item = local.getItem("theme");
  const darkItem = d.querySelector(".dark-mode");

  if (item !== null) {
    d.body.classList.add(item);
    darkItem.textContent = "🌞";
  } else {
    darkItem.textContent = "🌚";
  }
};

const darkModeBtn = (btn, darkClase) => {
  const darkBoton = d.querySelector(btn);
  const theme = local.getItem("theme") === null ? darkClase : local.getItem("theme");

  darkBoton.addEventListener("click", (e) => {
    if (e.target.matches(btn)) {
      d.body.classList.toggle(theme)
        ? local.setItem("theme", darkClase)
        : local.removeItem("theme");

      d.body.classList.contains(theme)
        ? (darkBoton.textContent = "🌞")
        : (darkBoton.textContent = "🌚");
    }
  });
};

export { darkModeBtn, Storage };
