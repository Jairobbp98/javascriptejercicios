const d = document, w = window;

const ScrollBotton = (btn) => {
    const $scrollBoton = d.querySelector(btn);

    w.addEventListener("scroll", e => {
        let scrollTop = w.pageYOffset || d.documentElement.scrollTop;

        if (scrollTop > 600) {
            $scrollBoton.classList.remove("desaparecer");
        } else {
            $scrollBoton.classList.add("desaparecer");
        }
    });

    d.addEventListener("click", e => {
        if (e.target.matches(btn) || e.target.matches(`${btn} *`)){
            w.scrollTo({
                behavior: "smooth",
                top: 0
            });
        }
    });
}

export { ScrollBotton }