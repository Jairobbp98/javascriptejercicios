import { darkModeBtn } from "./Modules/darkMode.js";
import { ScrollBotton } from "./Modules/topBotton.js";
const buttonHamburguer = document.getElementById("botonHamburguer");
const menu = document.querySelector(".menuLista");
const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  ScrollBotton(".boton-up");
  darkModeBtn(".dark-mode", "dark");
});

buttonHamburguer.addEventListener("click", () => {
  buttonHamburguer.classList.toggle("is-active");
  menu.classList.toggle("mostrarMenuLista");
});
