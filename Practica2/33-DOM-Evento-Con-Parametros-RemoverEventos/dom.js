const holaMundo = () => {
  alert("Hola Mundo");
  console.log(event);
};

const $eventoSemantico = document.getElementById("evento-semantico"),
  $eventoMultiple = document.getElementById("evento-multiple"),
  $eventoRemover = document.getElementById("evento-remover"); //no poner () porque el evento se ejecutara en cuanto la pagina se actualise. Estos eventos semanticos solo pueden tener un evento no se pueden cambiar. Por cada evento solop uede haber una funcion

/* $eventoSemantico.onclick = holaMundo; */ $eventoSemantico.onclick = (e) => {
  alert("Hola Mundo Manejador de Eventos Semánticos");
  console.log(e);
};

$eventoMultiple.addEventListener("click", holaMundo);
$eventoMultiple.addEventListener("click", (e) => {
  alert("Hola Mundo Manejador de Eventos Semánticos");
  console.log(e);
  console.log(e.type);
  console.log(e.target);
});

const saludar = (nombre = "Desconocido") => {
  alert(`Hola ${nombre}`);
};

$eventoMultiple.addEventListener("click", () => {
  saludar();
  saludar("Jon");
});

const removerDobleClick = (e) => {
  alert(`Removiendo el evento de tipo ${e.type}`);
  console.log(e);
  $eventoRemover.removeEventListener("dblclick", removerDobleClick);
};

$eventoRemover.addEventListener("dblclick", removerDobleClick);
