const holaMundo = () => {
  alert("Hola Mundo");
  console.log(event);
};

const $eventoSemantico = document.getElementById("evento-semantico"),
  $eventoMultiple = document.getElementById("evento-multiple"); //no poner () porque el evento se ejecutara en cuanto la pagina se actualise. Estos eventos semanticos solo pueden tener un evento no se pueden cambiar. Por cada evento solop uede haber una funcion

/* $eventoSemantico.onclick = holaMundo; */ $eventoSemantico.onclick = (e) => {
  alert("Hola Mundo Manejador de Eventos Semánticos");
  console.log(e);
};

$eventoMultiple.addEventListener("click", holaMundo);
$eventoMultiple.addEventListener("click", (e) => {
  alert("Hola Mundo Manejador de Eventos Semánticos");
  console.log(e);
  console.log(e.type);
  console.log(e.target);
});
