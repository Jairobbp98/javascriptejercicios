let id = Symbol(),
    id2 = Symbol();

console.log(id === id2);
console.log(id, id2);
console.log(typeof id, typeof id2);

const NOMBRE = Symbol();
const PERSONA = {
    [NOMBRE] : "jairo"
};

console.log(PERSONA);