let mapa = new Map();

mapa.set("Nombre", "Jon");
mapa.set("Apellido", "Barberena");
mapa.set("edad", 35);

console.log(mapa);
console.log(mapa.size);
console.log(mapa.has("correo")); //comprobar llave devuelve true o false
console.log(mapa.has("Nombre"));
console.log(mapa.get("Nombre"));
/* console.log(mapa.delete("Apellido")); */

for (let [key, value] of mapa) {
    console.log(`Llave: ${key}, Valor: ${value}`);
}

const llavesMapa2 = [...mapa.keys()];
const valoresMapa2 = [...mapa.values()];

console.log(llavesMapa2);
console.log(valoresMapa2);