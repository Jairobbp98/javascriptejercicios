//metodos que ya no se utilizan
console.log(document.documentElement.lang);
console.log(document.documentElement.getAttribute("lang"));
console.log(document.querySelector(".link-dom").href);
console.log(document.querySelector(".link-dom").getAttribute("href"));

/* document.documentElement.lang = "en"; */
document.documentElement.setAttribute("lang", "es-nic");

//$ hace referencia a variables q solo guardan elementos del dom
const $linkDOM = document.querySelector(".link-dom");

console.log($linkDOM);

$linkDOM.setAttribute("target", "_blank");
$linkDOM.setAttribute("rel", "noopener");
$linkDOM.setAttribute("href", "https://www.youtube.com/channel/UCyIcOQbvrnkBPFBHX4jQ6GQ/featured?view_as=subscriber");

console.log($linkDOM.hasAttribute("rel")); //devolvera true o false i el atriburo existe o no
$linkDOM.removeAttribute("rel"); //eliminar atributos
console.log($linkDOM.hasAttribute("rel"));

//data-attributes (html 5)

console.log($linkDOM.getAttribute("data-description"));
console.log($linkDOM.dataset);
console.log($linkDOM.dataset.description);
$linkDOM.dataset.description = "Suscribite a mi canal y comparte";
console.log($linkDOM.dataset.description);