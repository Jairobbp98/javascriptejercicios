const $divsEventos = document.querySelectorAll(".eventos-flujo div"),
      $linkEvento = document.querySelector(".eventos-flujo a");

function flujoEventos(e){
  console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
  e.stopPropagation(); //detiene la propagacion de los eventos
}

$divsEventos.forEach(div => {
  div.addEventListener("click", flujoEventos);
});

$linkEvento.addEventListener("click", (e) => {
  alert("me gusta el queso :v");
  e.preventDefault(); //previene la accion por defecto de la etiqueta
  e.stopPropagation();
})