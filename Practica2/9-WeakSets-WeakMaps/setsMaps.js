const ws = new WeakSet();
let valor1 = {"valor1": 1},
    valor2 = {"valor2": 2},
    valor3 = {"valor3": 3};

ws.add(valor1);
ws.add(valor2);
console.log(ws);

console.log(ws.has(valor1));
console.log(ws.has(valor2));

ws.delete(valor2);
console.log(ws);

ws.add(valor2);
ws.add(valor3);
console.log(ws);

/* const wm = new WeakMap([
    ["nombre", "kenai"],
    ["edad", 7],
    ["animal", "perro"],
    [null, "null"]
]); */

const wm = new WeakMap();
let llave1 = {};
let llave2 = {};
let llave3 = {};

wm.set(llave1, 1);
wm.set(llave2, 2);
console.log(wm);

console.log(wm.has(llave1));
console.log(wm.has(llave3));

console.log(wm.get(llave1));
console.log(wm.get(llave2));
console.log(wm.get(llave3));

wm.delete(llave2);
console.log(wm);