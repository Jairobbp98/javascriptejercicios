const $divsEventos = document.querySelectorAll(".eventos-flujo div"),
  $linkEvento = document.querySelector(".eventos-flujo a");

function flujoEventos(e) {
  console.log(
    `Hola te saluda ${t}, el click lo origino ${e.target.className}`
  );
}

document.addEventListener("click", (e) => {
  console.log("Click en ", e.target);

  if (e.target.matches(".eventos-flujo div")) {
    flujoEventos(e);
  }

  if (e.target.matches(".eventos-flujo a")) {
    alert("me gusta el queso :v");
    e.preventDefault();
  }
});