const $cards = document.querySelector(".cards");

console.log($cards);
console.log($cards.children); //devuelvo objeto con los hijos del elemento HTML
console.log($cards.children[2]); //devuelve la etiqueta del hijo indicado
console.log($cards.parentElement); //devuelve el elemento padre de nnuestra extiqueta html
console.log($cards.firstChild); //devuelve el nodo del primer hijo
console.log($cards.firstElementChild); //devuelve el primer elementi hijo
console.log($cards.lastChild); //devuelve el ultimo nodo hijo
console.log($cards.lastElementChild); //devuelve al ultimo hijo
console.log($cards.previousSibling); //devuelve al nodo hermano de arriba del padre
console.log($cards.previousElementSibling); //devuelve al elemento hermano
console.log($cards.nextElementSibling); //devuelve el ultimo elemento hermano
console.log($cards.children[3].closest("section"));