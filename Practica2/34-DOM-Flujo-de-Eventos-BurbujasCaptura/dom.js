const $divsEventos = document.querySelectorAll(".eventos-flujo div");

/* const flujoEventos = (e) =>{
  console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
} */

function flujoEventos(e){
  console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
}

$divsEventos.forEach(div => {
  //Fase de burbuja
  /* div.addEventListener("click", flujoEventos); */
  /* div.addEventListener("click", flujoEventos, false); */
  //fase de captura
  /* div.addEventListener("click", flujoEventos, true); */
  div.addEventListener("click", flujoEventos, {
    capture: true,
    once: true
  });
});