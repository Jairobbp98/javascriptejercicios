const $card = document.querySelector(".card");

console.log($card.classList);
console.log($card.className); //devuelve un texto con los nombres de las clases
console.log($card.classList.contains("rotate-45")); //devuelve tru o false si la clase existe dentro del elemento HTML
$card.classList.add("rotate-45"); // agrega nuevas clases al elemento HTML
console.log($card.classList.contains("rotate-45"));
console.log($card.className);
console.log($card.classList);
$card.classList.remove("rotate-45"); //remueve la clase del elemento HTML
console.log($card.classList);
$card.classList.toggle("rotate-45"); //si existe la clase la elimina y si no existe la crea
console.log($card.classList);
$card.classList.replace("rotate-45", "rotate-135");

$card.classList.add("opacity-80", "sepia");