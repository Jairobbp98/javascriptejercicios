//representa el viewport (dimension) de la pagina (solo la pagina)
window.addEventListener("resize", (e) => {
  console.log("--------------Evento resize--------------");
  console.log(window.innerWidth);
  console.log(window.innerHeight);
  console.log(window.outerWidth);
  console.log(window.outerHeight);
});

/* window.addEventListener("scroll", e =>{
  console.clear();
  console.log("--------------Evento resize--------------");
  console.log(window.scrollX);
  console.log(window.scrollY.toFixed(2));
  console.log(e);
}); */

window.addEventListener("load", e => {
  console.log("--------------Evento load--------------");
  console.log(window.screenX);
  console.log(window.screenY);
})

window.addEventListener("DOMContentLoaded", e => {
  console.log("--------------Evento DOMContentLoaded--------------");
  console.log(window.screenX);
  console.log(window.screenY);
});
