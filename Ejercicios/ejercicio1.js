//Programa una funcion que cuente el número de caracteres de una cadena de caracteres de una cadena de texto, pe.funcion("Hola Mundo") 10

function Contador(arr) {
    return arr.length;
}

console.log(Contador("Hola mi nombre es jairo"));

//solucion del profesor

//Solucion 1
function contarCadena(cadena = "") {
	if (!cadena) {
		console.warn("No ingresastes ninguna cadena");
	} else {
		console.info(`La cadena "${cadena}" tiene ${cadena.length} caracteres`);
	}
}

contarCadena("Hola mi amor");

//solucion 2
const contarCaracteres = (cadena = "") =>
(!cadena)
?console.warn("No ingresastes ninguna cadena")
:console.info(`La cadena "${cadena}" tiene ${cadena.length} caracteres`);