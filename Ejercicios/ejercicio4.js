//programa una funcio que repita un texto X veces, pe.miFuncion("Hola Mundo", 3) devolvera hola mundo hola mundo hola mundo

function Repetidor(Arreglo, repeticion) {
    let Nuevo = Arreglo.repeat(repeticion).split(',');
    return Nuevo;
}

function Repetidor2(Arreglo, repeticion) {
    let Nuevo = "";
    for (let i = 0; i < repeticion; i++) {
        Nuevo = Nuevo.concat(Arreglo, " ");
    }
    return Nuevo;
}

console.log(Repetidor2("Hola n.n", 5));

//Solución del profesor
const repetirTexto = (texto = "", veces = undefined) => {
    if (!texto) return console.warn("No ah ingresado ninguna cadena de texto");
    if (veces === undefined) return console.warn("No ingresastes el número de veces a repetir el texto");
    if (veces === 0) return console.error("El número de veces no puede ser 0");
    if (Math.sign(veces) === -1) return console.error("El número de veces no puede ser negativo");

    for (let i = 0; i < veces; i++) console.info(`${texto}, ${i}`);
}

repetirTexto("Hola Mundo", 5);