/*Programa una funcion que te devuelva el texto recortado según el número de caracteres indicado*/

function recortar(arreglo, num) {
    let resul;
    return resul = arreglo.slice(0, num);
}

console.log(recortar("Hola bb como estas todo bien?", 3));

//Solucion del profesor
const recortarTexto = (cadena = "", longitud = undefined) =>
(!cadena)
? console.warn("No ingresaste el texto")
: (longitud === undefined)
    ? console.warn("No ingresaste la longitud a recortar")
    : console.info(cadena.slice(0, longitud));

console.log(recortarTexto("Hola bb como estas todo bien", 6));