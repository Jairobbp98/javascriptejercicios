// Programa una función que elimine cierto patrón de caracteres de un texto dado, pe. miFuncion("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz") devolverá  "1, 2, 3, 4 y 5.

const eliminarPatron = (cadena = "", patron = "") => {
    if (!cadena) return console.warn("No ah ingresado ningun texto");
    if (!patron) return console.warn("No ah ingresado ningun patron");

    let expresionR = new RegExp(`${patron}`, 'ig');
    let nCadena = cadena.replace(expresionR, "");

    return console.log(nCadena);
}

eliminarPatron("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz");

//Solucion del profesor
const eliminarCaracteres = (texto = "", patron = "") =>
    (!texto)
        ?console.warn("No ingresastes un texto")
        :(!patron)
            ?console.warn("No ingresastes un patron de caracteres")
            :console.info(texto.replace(new RegExp(patron, "ig"), ""));

eliminarCaracteres("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz");