// Programa una función que valide si una palabra o frase dada, es un palíndromo (que se lee igual en un sentido que en otro), pe. mifuncion("Salas") devolverá true.

const cadenaPalindromo = (cadena = "") => {
    if (!cadena) return console.warn("No ah ingresado ningun texto");

    let cadenaMinuscula = cadena.toLowerCase();
    let normal = cadenaMinuscula.split("");
    let cInversa = cadenaMinuscula.split("").reverse();
    let count = 0;
    
    for(let i = 0; i < normal.length; i++) 
        if (cInversa[i] === normal[i]) 
            count++;

    if (count === normal.length) {
        return console.log(`La palabra ${cadena} es una palabra Palíndromo`);
    }else{
        return console.info("Esta palabra no es palíndromo");
    }
}

cadenaPalindromo("Menem");

//solucion del profesor

const palindromo = (palabra = "") => {
    if (!palabra) return console.warn("No ingresastes una palabra o frase");

    palabra = palabra.toLowerCase();
    let alReves = palabra.split("").reverse().join("");

    return (palabra === alReves)
        ?console.info(`Si es palíndromo, Palabra original ${palabra}, Palabra al revés ${alReves}`)
        : console.info(`No es palíndromo, Palabra original ${palabra}, Palabra al revés ${alReves}`);
}

palindromo("jairo");