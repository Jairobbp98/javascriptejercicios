//Programa una función para contar el número de veces que se repite una palabra en un texto largo, pe. miFuncion("hola mundo adios mundo", "mundo") devolverá 2.

const buscadorPalabras = (cadena = "", repeticion = "") => {
    if (!cadena) return console.warn("No ah ingresado ningun texto");
    if (!repeticion) return console.warn("No ah ingresado ningun texto");

    cadena = cadena.trim().replace(/[^a-zA-Z0-9]/g, " ");//elimina espacios en blanco y elimina caracteres especiales

    let nCadena = cadena.split(" "), resultado = 0;

    for (let i = 0; i < nCadena.length; i++) if (repeticion === nCadena[i]) resultado++;
    
    return console.log(`La palabra se repite ${resultado} veces`);
}

buscadorPalabras("Hola, Hola Hola Hola como vas", "Hola");

//Solucion del profesor
const textoEnCadena = (cadena = "", texto = "") => {
    if (!cadena) return console.warn("No ah ingresado ningun texto");
    if (!texto) return console.warn("No ah ingresado ningun texto");

    let i = 0, contador = 0;

    while (i !== -1) {
        i = cadena.indexOf(texto, i);
        if(i !== -1){
            i++;
            contador++;
        }
    }
    return console.info(`La palabra ${texto} se repite ${contador} veces`);
}

textoEnCadena("hola mundo adios mundo", "mundo");