//Programa una función que invierta las palabras de una cadena de texto, pe. miFuncion("Hola Mundo") devolverá "odnuM aloH".

function stringReverso(cadena) {
    let nCadena = "";
    for (let i = cadena.length - 1; i >= 0; i--) {
        nCadena += cadena[i];
    }
    return console.log(nCadena);
}

const stringReversoC = (cadena = "") =>
(!cadena)
? console.warn("No ah ingresado ningun texto")
: console.info(cadena.split("").reverse().join(""));

stringReverso("Hola");
stringReversoC("Hola");