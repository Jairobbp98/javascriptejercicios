//24) Programa una función que dado un arreglo de números devuelva un objeto con dos arreglos, el primero tendrá los numeros ordenados en forma ascendente y el segundo de forma descendiente, pe. miFuncion([7, 5,7,8,6]) devolverá { asc: [5,6,7,7,8], desc: [8,7,7,6,5] }.

const ordenNumerico = (arreglo) =>{
    if(arreglo === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arreglo instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arreglo.length === 0) return console.error("El arreglo esta vacío");

    for(let num of arreglo){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }

    let numeros = {
        ascendentes: (x = arreglo) =>{
            return x.sort((a, b) => a - b)
        },
        descendentes: (x = arreglo) =>{
            return x.sort((a, b) => b - a)
        }
    };

    return console.log(`Ascendentes: ${numeros.ascendentes()}\nDescendentes: ${numeros.descendentes()}`);
}

ordenNumerico([7,3,8,4,9,5,1,2,6,10]);

const ordenarArreglo = (arr = undefined) =>{
    if(arr === undefined) return console.warn("No ingresastes un arreglo de numeros");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("EL arreglo esta vacio");

    for(let num of arr){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }

    return console.info({
        arr,
        asc: arr.map(el => el).sort(),
        desc: arr.map(el => el).sort().reverse()
    });
}

ordenarArreglo([7,3,8,4,9,5,1,2,6,10]);