//26) Programa una función que dado un arreglo de números obtenga el promedio, pe. promedio([9,8,7,6,5,4,3,2,1,0]) devolverá 4.5.

const promedioArreglo = (arr) =>{
    
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    for(let num of arr){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }

    let resultado = 0

    for (let i = 0; i < arr.length; i++) {
        resultado += arr[i]
    }

    return console.log("El promedio es: " + (resultado = resultado / arr.length));
}

promedioArreglo([9,8,7,6,5,4,3,2,1,0]);

const promedio = (arr = undefined) =>{

    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    for(let num of arr){
        if(typeof num !== "number") return console.error(`El valor"${num}" ingresado, NO es un número`);
    }
    
    return console.info(
        arr.reduce((total, num, index, arr) =>{
            total += num;
            if(index === arr.length - 1){
                return `El promedio de ${arr.join(" + ")} es ${total / arr.length}`;
            }else{
                return total;
            }
        })
    );
}

promedio([9,8,7,6,5,4,3,2,1,0]);