//25) Programa una función que dado un arreglo de elementos, elimine los duplicados, pe. miFuncion(["x", 10, "x", 2, "10", 10, true, true]) devolverá ["x", 10, 2, "10", true].

const elementosDuplicados = (arr) =>{
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0) return console.error("El arreglo esta vacío");

    let resultado = arr.filter((valorActual, indiceActual, arreglo) =>
        arreglo.indexOf(valorActual) === indiceActual
    );

    return console.log(resultado);
}

elementosDuplicados(["x", 10, "x", 2, "10", 10, true, true]);

//solucion del profesor

const quitarDuplicados = (arr = undefined) =>{
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0 || arr.length === 1) return console.error("El arreglo esta vacío");

    return console.info({
        original: arr,
        sinDuplicados: arr.filter((value, index, self) =>
            self.indexOf(value) === index
        )
    });
}

/* quitarDuplicados(["x", 10, "x", 2, "10", 10, true, true]); */

//solucion del profesor ECMASCRIPT6

const quitarDuplicadosNuevo = (arr = undefined) =>{
    if(arr === undefined) return console.warn("El valor que ingresastes no es un arreglo");

    if(!(arr instanceof Array)) return console.error("El valor que ingresastes no es un arreglo");

    if(arr.length === 0 || arr.length === 1) return console.error("El arreglo esta vacío");

    return console.info({
        original: arr,
        sinDuplicados: [...new Set(arr)]
    });
}

quitarDuplicadosNuevo(["x", 10, "x", 2, "10", 10, true, true]);